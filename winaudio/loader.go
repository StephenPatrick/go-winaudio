// +build windows

package winaudio

import (
	bin "encoding/binary"
	"io"

	"github.com/oov/directsound-go/dsound"
)

// These should be changable in the future
const (
	SampleRate  = 44100
	Bits        = 16
	Channels    = 2
	BlockAlign  = Channels * Bits / 8
	BytesPerSec = SampleRate * BlockAlign
)

func LoadWav(filename string) (Audio, error) {
	// Load a wave audio file onto a secondary buffer.
	dsbuff, err := loadWaveFile(filename)
	if err != nil {
		return nil, err
	}
	return &DSbuffer{dsbuff, 0}, nil
}

// The loadWaveFile function is what handles loading in a .wav audio file and then copies the data onto a new secondary buffer.

func loadWaveFile(filename string) (*dsound.IDirectSoundBuffer, error) {
	w, err := readWavFile(filename)
	if err != nil {
		return nil, err
	}
	return writeBuffer(w.data, w.Subchunk2Size)
}

func writeBuffer(wave []byte, wavelen uint32) (*dsound.IDirectSoundBuffer, error) {
	if wavelen == 0 {
		wavelen = uint32(len(wave))
	}

	wf := dsound.WaveFormatEx{
		FormatTag:      dsound.WAVE_FORMAT_PCM,
		Channels:       Channels,
		SamplesPerSec:  SampleRate,
		BitsPerSample:  Bits,
		BlockAlign:     Channels * Bits / 8,
		AvgBytesPerSec: BytesPerSec,
		ExtSize:        0,
	}

	buffdsc := dsound.BufferDesc{
		// These flags cover everything we should ever want to do
		Flags:       dsound.DSBCAPS_GLOBALFOCUS | dsound.DSBCAPS_GETCURRENTPOSITION2 | dsound.DSBCAPS_CTRLVOLUME | dsound.DSBCAPS_CTRLPAN | dsound.DSBCAPS_CTRLFREQUENCY | dsound.DSBCAPS_LOCDEFER,
		Format:      &wf,
		BufferBytes: wavelen,
	}

	// Create the object which stores the wav data in a playable format
	dsbuff, err := ds.CreateSoundBuffer(&buffdsc)
	if err != nil {
		return nil, err
	}

	// Reserve some space in the sound buffer object to write to.
	// The Lock function (and by extension LockBytes) actually
	// reserves two spaces, but we ignore the second.
	by1, by2, err := dsbuff.LockBytes(0, uint32(len(wave)), 0)
	if err != nil {
		return nil, err
	}

	// Write to the pointer we were given.
	copy(by1, wave)

	// Update the buffer object with the new data.
	err = dsbuff.UnlockBytes(by1, by2)
	if err != nil {
		return nil, err
	}
	return dsbuff, nil
}

// What follows is an abbreviated form of verdverm's go-wav library, which was copied
// as we required access to the unexported data field

type WavData struct {
	bChunkID  [4]byte // B
	ChunkSize uint32  // L
	bFormat   [4]byte // B

	bSubchunk1ID  [4]byte // B
	Subchunk1Size uint32  // L
	AudioFormat   uint16  // L
	NumChannels   uint16  // L
	SampleRate    uint32  // L
	ByteRate      uint32  // L
	BlockAlign    uint16  // L
	BitsPerSample uint16  // L

	bSubchunk2ID  [4]byte // B
	Subchunk2Size uint32  // L
	data          []byte  // L
}

func readWavFile(fn string) (WavData, error) {
	file, err := Open(fn)
	if err != nil {
		return WavData{}, err
	}
	defer file.Close()
	return readWavData(file)
}

func readWavData(r io.Reader) (WavData, error) {
	wav := WavData{}

	bin.Read(r, bin.BigEndian, &wav.bChunkID)
	bin.Read(r, bin.LittleEndian, &wav.ChunkSize)
	bin.Read(r, bin.BigEndian, &wav.bFormat)

	bin.Read(r, bin.BigEndian, &wav.bSubchunk1ID)
	bin.Read(r, bin.LittleEndian, &wav.Subchunk1Size)
	bin.Read(r, bin.LittleEndian, &wav.AudioFormat)
	bin.Read(r, bin.LittleEndian, &wav.NumChannels)
	bin.Read(r, bin.LittleEndian, &wav.SampleRate)
	bin.Read(r, bin.LittleEndian, &wav.ByteRate)
	bin.Read(r, bin.LittleEndian, &wav.BlockAlign)
	bin.Read(r, bin.LittleEndian, &wav.BitsPerSample)

	bin.Read(r, bin.BigEndian, &wav.bSubchunk2ID)
	bin.Read(r, bin.LittleEndian, &wav.Subchunk2Size)

	wav.data = make([]byte, wav.Subchunk2Size)
	bin.Read(r, bin.LittleEndian, &wav.data)

	return wav, nil
}
