// +build windows

package winaudio

import (
	"errors"
	"syscall"

	"github.com/oov/directsound-go/dsound"
)

const (
	// SetVolume takes, hypothetically, an int32 and
	// scales it to a decibel level for an audio source
	// on a (-10000 -- 0) scale, with 0 being max volume.
	// This is ludicrous, and should be changed with
	// further research into what the acceptable values are--
	// MSDN says the values are (0 -- 10000).
	MAX_VOLUME = 0
	MIN_VOLUME = -10000
	// SetPan takes a range between -10000 and 10000,
	// representing left and right pan respectively.
	RIGHT_PAN = 10000
	LEFT_PAN  = -10000
	// SetFrequency takes a range between 100 and 10000.
	// Probably doesn't need modifying in most cases.
	MIN_FREQUENCY = 100
	MAX_FREQUENCY = 100000
)

func InitializeDirectSound() (*dsound.IDirectSound, error) {
	hasDefaultDevice := false
	dsound.DirectSoundEnumerate(func(guid *dsound.GUID, description string, module string) bool {
		if guid == nil {
			hasDefaultDevice = true
			return false
		}
		return true
	})
	if !hasDefaultDevice {
		return nil, errors.New("No default device available to play audio off of")
	}

	ds, err := dsound.DirectSoundCreate(nil)
	if err != nil {
		return nil, err
	}

	desktopWindow, _, err := getDesktopWindow.Call()
	err = ds.SetCooperativeLevel(syscall.Handle(desktopWindow), dsound.DSSCL_PRIORITY)
	if err != nil {
		return nil, err
	}

	return ds, nil
}

type DSbuffer struct {
	*dsound.IDirectSoundBuffer
	// We store flags here in case we ever
	// want to use more than looping.
	// these are referred to on play.
	flags dsound.BufferPlayFlag
}

func (db *DSbuffer) SetLooping(loop bool) {
	if loop {
		db.flags = dsound.DSBPLAY_LOOPING
	} else {
		db.flags = 0
	}
}

func (db *DSbuffer) Pause() error {
	return db.IDirectSoundBuffer.Stop()
}

// Stop pauses and resets the buffer
// position to the beginning of the audio.
func (db *DSbuffer) Stop() error {
	err := db.IDirectSoundBuffer.Stop()
	if err != nil {
		return err
	}
	return db.IDirectSoundBuffer.SetCurrentPosition(0)
}

func (db *DSbuffer) Resume() error {
	errChan := make(chan error)
	// In testing, calling play without using a goroutine
	// hasn't functioned.
	go func(dsbuff *dsound.IDirectSoundBuffer, flags dsound.BufferPlayFlag, errChan chan error) {
		err := dsbuff.Play(0, flags)
		if err != nil {
			errChan <- err
		} else {
			errChan <- nil
		}
	}(db.IDirectSoundBuffer, db.flags, errChan)
	return <-errChan
}

// Play resumes, but resets the buffer
// position to the beginning of the audio first.

func (db *DSbuffer) Play() error {
	errChan := make(chan error)
	go func(dsbuff *dsound.IDirectSoundBuffer, flags dsound.BufferPlayFlag, errChan chan error) {
		err := dsbuff.SetCurrentPosition(0)
		if err != nil {
			errChan <- err
		} else {
			err = dsbuff.Play(0, flags)
			if err != nil {
				errChan <- err
			} else {
				errChan <- nil
			}
		}
	}(db.IDirectSoundBuffer, db.flags, errChan)
	return <-errChan
}
