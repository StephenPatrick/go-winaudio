package winaudio

import (
	"math"
)

// Unfinished!
func Sin(freq, seconds float64, volume uint8) (Audio, error) {
	wave := make([]byte, int(seconds*SampleRate))
	for i := range wave {
		wave[i] = byte(float64(volume) * math.Sin(freq*(float64(i)/SampleRate)*2*math.Pi))
	}
	dsbuff, err := writeBuffer(wave, 0)
	return &DSbuffer{dsbuff, 0}, err
}
