// +build windows

// The winaudio package provides a basic layer of access to play, load,
// and manipulate wav files through DirectSound.
package winaudio

// This code was put together through a combination of
// oov's dsound library (imported),
// this direct sound tutorial page http://www.rastertek.com/dx11tut14.html (author unknown)
// and verdverm's go-wav library (which is copied as we need access to a private field)

import (
	"syscall"

	"github.com/oov/directsound-go/dsound"
)

var (
	user32           = syscall.NewLazyDLL("user32")
	getDesktopWindow = user32.NewProc("GetDesktopWindow")
	ds               *dsound.IDirectSound
	err              error
)

type Audio interface {
	Pause() error
	Stop() error
	Resume() error
	Play() error
	SetLooping(loop bool)
	SetVolume(volume int32) error
	GetVolume() (int32, error)
	SetPan(pan int32) error
	GetPan() (int32, error)
	SetFrequency(freq uint32) error
	GetFrequency() (uint32, error)
}

func InitWinAudio() error {
	ds, err = InitializeDirectSound()
	return err
}
