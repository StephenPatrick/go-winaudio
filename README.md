## This library is deprecated ##

Please use github.com/200sc/klangsynthese for a much nicer API, additional features, and fewer bugs

The original readme follows

# README #

This library is a combination of the work of oov and verdverm's dsound and go-wav libraries to create a simple audio interface using windows Direct Sound in native go. 

This currently only supports playing wav files, but ways to use other audio formats will be sought out.

The need for this library arose for me when I noticed most game engines in go had absent audio functionality for windows.

# USAGE #


```
#!

go get -u bitbucket.org/StephenPatrick/go-winaudio/winaudio
```



```
#!go

...
winaudio.InitWinAudio()
wavfile, _ := winaudio.LoadWav("../audio/wavfile.wav")
wavfile.SetLooping(true)
wavfile.Play()

```

See the audio interface in winaudio.go for a complete set of functions